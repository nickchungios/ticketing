import { Subjects, Publisher, PaymentCreatedEvent } from '@nctickets/common';

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
  readonly subjcet = Subjects.PaymentCreated;
}