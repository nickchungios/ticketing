import { Publisher, Subjects, TicketCreatedEvent } from '@nctickets/common';

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent>{
  readonly subjcet = Subjects.TicketCreated;
}