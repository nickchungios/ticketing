import { Publisher, Subjects, TicketUpdatedEvent } from '@nctickets/common';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  readonly subjcet = Subjects.TicketUpdated;
}