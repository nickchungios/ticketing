import { Publisher, OrderCreatedEvent, Subjects } from '@nctickets/common';

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent> {
  readonly subjcet = Subjects.OrderCreated;
}