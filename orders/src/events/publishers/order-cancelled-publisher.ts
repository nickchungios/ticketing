import { Subjects, Publisher, OrderCancelledEvent } from '@nctickets/common';

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent>{
  readonly subjcet = Subjects.OrderCancelled;
}