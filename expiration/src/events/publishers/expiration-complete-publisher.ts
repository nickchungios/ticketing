import { Subjects, Publisher, ExpirationCompleteEvent } from '@nctickets/common';

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
  readonly subjcet = Subjects.ExpirationComplete;
}